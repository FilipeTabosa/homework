var player;

var canvas = document.getElementById('game');

var context = canvas.getContext('2d');

// draws the game map; values in the matrix are interpreted as follows:
//   - 0 : empty space
//   - 1 : a "wall"
//   - 2 : the "treasure"
//   - 3 : a player
//   - 4 : the winning player position
//
//  the current player position is also drawn on the map
var draw = function (map) {
  canvas.width = canvas.width; // clear the canvas

  var gridHeight = map.length;
  var gridWidth  = map[0].length;
  var pacman = document.createElement('img');
  pacman.src = 'http://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Pac_Man.svg/200px-Pac_Man.svg.png';
  var block = document.createElement('img');
  block.src = 'http://i.imgur.com/eyRiia4.jpg';

  var cellWidth = canvas.width / gridWidth;
  var cellHeight = canvas.height / gridHeight;

  // draw the map
  for (var row=0; row<map.length; row++) {
    for (var col=0; col<map[row].length; col++) {
      if (map[row][col] !== 0) {
        switch (map[row][col]) {
        case 1: context.fillStyle = 'black';  break;
        case 2: context.fillStyle = 'yellow'; break;
        case 3: context.fillStyle = 'green';  break;
        case 4: context.fillStyle = 'blue';  break;
        }

        context.fillRect(cellWidth * col, cellHeight * row,
                         cellWidth, cellHeight);
      }
    }
  }

  // draw the player
   context.drawImage(pacman, cellWidth * player.x, cellHeight * player.y, cellWidth, cellHeight);
  
//context.fillStyle = 'red';
  //context.fillRect(cellWidth * player.x, cellHeight * player.y,
    //               cellWidth, cellHeight);

  // outline the map
  context.strokeRect(0, 0, canvas.width, canvas.height);
};

var socket = io(); // establish a connection to the server

// on receiving a "game update" message from the server, redraw the map
socket.on('game update', function (data) {
  // if we are given a new player position, keep track of it
  if (data.pos) {
    player = data.pos;
  }
  draw(data.map);
});

// each supported keystroke sends a message to the server
var keyHandler = function (key) {
  switch (key.keyCode) {
  case 37: // left
    socket.emit('player move', { dx: -1, dy:  0 });
    break;
  case 38: // up
    socket.emit('player move', { dx:  0, dy: -1 });
    break;
  case 39: // right
    socket.emit('player move', { dx:  1, dy:  0 });
    break;
  case 40: // down
    socket.emit('player move', { dx:  0, dy:  1 });
    break;

  case 82: // 'r' -- randomize the map
  //var rng = setInterval(function(){
 socket.emit('randomize map', true);
  // },3000);  
}  
 
  key.preventDefault(); // prevent cursor keys from scrolling the browser viewport
};

// listen for key presses in the browser window
window.addEventListener('keydown', keyHandler);


// I've won! Draw message, disconnect from server, stop processing keys.
socket.on('win', function () {
  context.font = '36pt Helvetica';
  context.fillStyle = 'green';
  context.fillText('You win!', 35, 150);
  window.removeEventListener(keyHandler);
  socket.disconnect();
});

// I've lost! Draw message, disconnect from server, stop processing keys.
socket.on('lose', function (data) {
  draw(data.map);
  context.font = '36pt Helvetica';
  context.fillStyle = 'red';
  context.fillText('You lose!', 28, 150);
  window.removeEventListener(keyHandler);
  socket.disconnect();
});
