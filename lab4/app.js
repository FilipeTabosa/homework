var http = require('http');
var fs = require('fs');

var SERVER_PORT = 19195; // use the last 4 digits of your ID preceded by a 1
var SITE_ROOT   = '../www';

var server = http.createServer(function (req, res) {
  // serve up the static files (HTML and JavaScript)
  var url = require('url').parse(req.url, true);
  if (url.pathname === '/game' || url.pathname === '/') {
    res.writeHead(200, { 'Content-type' : 'text/html' });
    res.end(fs.readFileSync(SITE_ROOT + '/game.html'));
  } else {
    res.writeHead(200);
    fs.readFile(SITE_ROOT + url.pathname, function (err, data) {
      if (err) {
	res.end('File not found!');
      } else {
	res.end(data);
      }
    });
  }
});

server.listen(SERVER_PORT); // accept HTTP connections

var map = [];

var player_positions = {};

var map_width = 15;
var map_height = 15;

// populate the map with a (map_width x map_height) matrix of random values
// selected from the following:
//   - 0 : empty space
//   - 1 : a "wall"
//   - 2 : the "treasure"
var randomize_map = function () {
  map = [];

  // build a random map of empty spaces and walls
  for (var r=0; r<map_height; r++) {
    var row = [];
    for (var c=0; c<map_width; c++) {
      row.push(Math.floor(Math.random() * 2));
    }
    map.push(row);
  }

  // make sure players, if there are any, are on empty spaces
  Object.keys(player_positions).forEach(function (id) {  
    var pos = player_positions[id];
    map[pos.y][pos.x] = 0;
  });

  // randomly place the treasure on space that contained a wall
  var randX, randY;
//first try, duplicating the code  
// var secondX, secondY; 
//a random number of trasure between 1 and 4
  for (var i=0; i<((Math.random() *4 ) ); i++){
  do {
    randX = Math.floor(Math.random() * map_width);
    randY = Math.floor(Math.random() * map_height);
  } while (map[randY][randX] != 1);
  map[randY][randX] = 2;
}
 //first I tried to do it duplicating the code, but using a for is easier
  //do {
   // secondX = Math.floor(Math.random() * map_width);
   // secondY = Math.floor(Math.random() * map_height);
 // } while (map[secondY][secondX] != 1);
//  map[secondY][secondX] = 2;
};


// generate a random map on server startup
randomize_map ();

// returns a map matrix with player positions marked by the value '3'
var map_with_users = function () {
  // copy the original map
  var user_map = [];
  for (var i=0; i<map.length; i++) {
    user_map.push(map[i].slice());
  }

  // overlay users on it
  Object.keys(player_positions).forEach(function (id) {
    var pos = player_positions[id];
    user_map[pos.y][pos.x] = 3;
  });
  
  return user_map;
};

var io = require('socket.io')(server); // accept game messages

// happens whenever a client (browser) connects
io.on('connection', function (socket) {
  console.log('   Connected client; num connected = '
              + (Object.keys(player_positions).length + 1));

  // come up with a valid random position on the map for the player
  var randX, randY;
  do {
    randX = Math.floor(map_width * Math.random());
    randY = Math.floor(map_height * Math.random());
  } while (map[randY][randX] != 0);

  // record it in the global table
  player_positions[socket.id] = { x: randX, y: randY };

  // send the client its initial position and the current map
  socket.emit('game update',
              { pos: player_positions[socket.id],
                map: map_with_users() });

  // tell all other clients about this new player
  socket.broadcast.emit('game update',
                        { map: map_with_users() });

  // happens whenever we get a "player move" message from a client
  // --- the client should give us on object containing the position
  // change expressed as { dx: M, dy: N }
  socket.on('player move', function (data) {
    var currpos = player_positions[socket.id];
    var newpos  = { x: currpos.x + data.dx,
                    y: currpos.y + data.dy };
    if (newpos.x < 0 || newpos.x >= map_width
        || newpos.y < 0 || newpos.y >= map_height
        || map[newpos.y][newpos.x] == 1) {
      // skip if the motion is invalid (run off the map or into a wall)
      return;
    }

    // record the new position
    player_positions[socket.id] = { x: currpos.x + data.dx,
                                    y: currpos.y + data.dy };
    // tell the client its new position
    socket.emit('game update',
                { pos: player_positions[socket.id],
                  map: map_with_users() });

    // tell all other clients about the updated map
    socket.broadcast.emit('game update',
                          { map: map_with_users() });

    // if this client's player position is over the treasure,
    // trigger the win & loss messages, then regenerate the map
    // --- we assume here that all clients disconnect on receiving
    // the win/loss messages
    if (map[newpos.y][newpos.x] == 2) {
      map[newpos.y][newpos.x] = 4; // mark winning position
      socket.emit('win');
      socket.broadcast.emit('lose',
                            { map: map });
      randomize_map();
    }
  });

  // regenerate the map and tell all clients about it
  socket.on('randomize map', function (data) {
    randomize_map();
    io.emit('game update',
            { map: map_with_users() });
  });

  // delete player position when client disconnects
  socket.on('disconnect', function (data) {
    delete player_positions[socket.id];
    socket.broadcast.emit('game update',
                          { map: map_with_users() });
    console.log('Disconnected client; num connected = '
                + Object.keys(player_positions).length);
  });
});
